.model small


.stack 64
.data
  
;declarando variable global 
numero1 db 3
numero2 db 2
multiplicacion db 0


;mensaje para mostrar los resultados
 
msjnM db 10,13, "La multiplicacion es: ",'$'


.code
begin proc far
    
    ;direccionamiento del procedimiento
    mov ax, @data
    mov ds,ax
            
    ;MULTIPLICACION
    mov al,numero1
    mul numero2
    mov multiplicacion,al 

    ;mostrando la multiplicacion
    mov ah,09
    lea dx,msjnM
    int 21h
    mov dl,multiplicacion
    add dl,30h 
    mov ah,02
    int 21h
      
    ;cierre del programa
    mov ah,4ch
    int 21h
    
    begin endp
end