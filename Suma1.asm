.model small


.stack 64
.data
  
;declarando variable global
suma db 0


;mensaje para mostrar los resultados
 
msjnS db 10,13, "La suma es: ",'$'


.code
begin proc far
    
    ;direccionamiento del procedimiento
    mov ax, @data
    mov ds,ax
            
    ;SUMA             
    mov al,5h
    add al,4h
    mov suma,al  

    ;mostrando la suma
    mov ah,09
    lea dx,msjnS
    int 21h
    mov dl,suma
    add dl,30h 
    mov ah,02
    int 21h  
      
    ;cierre del programa
    mov ah,4ch
    int 21h
    
    begin endp
end