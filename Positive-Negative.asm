.model small ; modelo de memoria
.stack 64 ; tama�o de la pila
.data
var1 db ?
msg1 db "INGRESE NUMERO: $"
msg2 db "=>EL NUMERO INGRESADO ES POSITIVO: $"
msg3 db "=>EL NUMERO INGRESADO ES NEGATIVO: $"
msg4 db "=>EL NUMERO INGRESADO ES CERO: $"
.code
start:
mov ah,09h
lea dx, msg1
int 21h
mov ah,01h
int 21h
sub al,30h
mov var1,al
mov al,0h
cmp var1,al
ja mayor
jb menor
je igual
mayor:
mov ah,09h
lea dx,msg3
int 21h
jmp salir
menor:
mov ah,09h
lea dx,msg2
int 21h
jmp salir
igual:
mov ah,09h
lea dx,msg4
int 21h
jmp salir
salir:
.exit
end