.MODEL SMALL
.STACK 64
.DATA
ESPACIO DB 13,10,"$"
LECTURA DB 13,10,"PROGRAMA QUE CONSISTE EN LEER UN NUMERO DE UN DIGITO POR TECLADO","$"
MENSAJE DB 13,10,"DIGITAR NUMERO DE UN SOLO DIGITO: ","$"
MEN_CERO DB 13,10,"EL NUMERO INGRESADO ES CERO","$"
ESPACIO1 DB 13,10,"","$"
VALOR DB 0
NUM DB 2

.CODE
.STARTUP
MOV AX, @DATA
MOV AH,09H
LEA DX,ESPACIO
INT 21H
MOV AH,09H
LEA DX,LECTURA
INT 21H
MOV AH,09H
LEA DX,ESPACIO
INT 21H
MOV AH,09H
LEA DX, MENSAJE
INT 21H
CALL LEER
SUB AL,30H
MOV VALOR,AL
MOV AH,09H
LEA DX,ESPACIO
INT 21H
MOV BL,VALOR
CMP BL,0
JE CERO
INT 21H
XOR AX,AX
MOV AL,NUM
MOV BL,AL
MOV AL,VALOR
DIV BL
CMP AH,0

;PROCEDIMIENTOS
LEER PROC NEAR
MOV AH,01H
INT 21H
RET
CERO:
MOV AH,09H
LEA DX,MEN_CERO
INT 21H
JMP SALIR

SALIR:
MOV AX,4C00H
INT 21H
END